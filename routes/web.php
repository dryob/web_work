<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
})->name('home');
Route::get('event', function () {
    return view('frontend.event');
})->name('event');
Route::get('member', function () {
    return view('frontend.member');
})->name('member');
Route::get('login', function () {
    return view('frontend.login');
})->name('login');

Route::get('newnews', function () {
    return view('frontend.newnews');
})->name('newnews');

Route::get('newmembercsv', function () {
    return view('frontend.newmembercsv');
})->name('newmembercsv');

Route::get('newmember', function () {
    return view('frontend.newmember');
})->name('newmember');

Route::get('newevent', function () {
    return view('frontend.newevent');
})->name('newevent');

Route::get('register', function () {
    return view('frontend.register');
})->name('register');
