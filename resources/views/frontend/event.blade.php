@extends('frontend.layouts.master')
@section('title', 'Event')
@section('nav_event', 'active')
@section('content')
<table class="table table-hover table-striped">
  <thead class="table-primary">
    <tr>
      <th>#</th>
      <th>活動名稱</th>
      <th>活動簡介</th>
      <th>活動日期</th>
      <th>活動地點</th>
      <th>報名</th>
      <th>下載報名名單(admin only)<a href="newevent" class="btn btn-primary">新增活動</a></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <th>活動1</th>
      <th>活動1簡介</th>
      <th>2018/11/05</th>
      <th>地點</th>
      <th>
      <div class="btn-group">
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
          報名
          </button>
            <div class="dropdown-menu">
            <a class="dropdown-item" href="#!">報名</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#!">退出</a>
            </div>
          </div>
      </th>
      <th>
      <a href="#!" class="btn btn-outline-primary">
        <span class="fa fa-download"></span>
      </a>
      </th>
    </tr>

  </tbody>
</table>
@endsection
