@extends('frontend.layouts.master')
@section('title', 'login')
@section('nav_login', 'active')
@section('content')
<form>
  <div class="form-group">
    <label for="account">學號</label>
    <input class="form-control" type="text" id="account">
    <label for="password">密碼</label>
    <input class="form-control" type="password" id="password">

  </div>
  <button type="submit" class="btn btn-primary">登入</button>
  <a href="register" class="btn btn-primary">註冊</a>

</form>

@endsection
