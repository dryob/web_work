@extends('frontend.layouts.master')
@section('title', 'newmembercsv')
@section('nav_member', 'active')
@section('content')
<form>
  <div class="form-group">
  <div class="custom-file">
  <input type="file" class="custom-file-input" id="csvFile">
  <label class="custom-file-label" for="csvFile">Choose file(csv)</label>
  </div>

  </div>
  <button type="submit" class="btn btn-primary">新增</button>
</form>

@endsection
