@extends('frontend.layouts.master')
@section('title', 'newevent')
@section('nav_event', 'active')
@section('content')
<form>
  <div class="form-group">
    <label for="event_name">活動名稱</label>
    <input class="form-control" type="text" id="event_name">
    <label for="name">活動簡介</label>
    <input class="form-control" type="text" id="event_content">
    <label for="event_date">活動日期</label>
    <input class="form-control" type="date" id="event_date">
    <label for="event_location">活動地點</label>
    <input class="form-control" type="text" id="event_location">
  </div>
  <button type="submit" class="btn btn-primary">新增</button>
</form>

@endsection
