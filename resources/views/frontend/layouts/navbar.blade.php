<nav class="navbar navbar-expand-lg navbar-dark bg-secondary">
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<a class="navbar-brand" href="{{ route('home')}}">系學會網頁系統</a>

<div class="collapse navbar-collapse" id="navbarTogglerDemo03">
<ul class="navbar-nav mr-auto mt-2 mt-md-0 ">
    <li class="nav-item @yield('nav_home')">
    <a class="nav-link" href="{{ route('home')}}">最新消息
        <!-- <span class="sr-only">(current)</span> -->
    </a>
    </li>
    <li class="nav-item @yield('nav_event')">
    <a class="nav-link" href="{{ route('event')}}">活動</a>
    </li>
    <li class="nav-item @yield('nav_member')">
    <a class="nav-link" href="{{ route('member')}}">會員管理</a>
    </li>
    <li class="nav-item @yield('nav_login')">
    <a class="nav-link" href="{{ route('login')}}">登入</a>
    </li>

</ul>
</div>
</nav>