@extends('frontend.layouts.master')
@section('title', 'Member')
@section('nav_member', 'active')
@section('content')
<table class="table table-hover table-striped">
  <thead class="table-primary">
    <tr>
      <th>#</th>
      <th>學號</th>
      <th>姓名</th>
      <th>權限</th>
      <th>
      <div class="btn-group">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
    aria-expanded="false">
    新增會員
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="newmember">手動</a>
    <a class="dropdown-item" href="newmembercsv">批次</a>
  </div>
</div>
</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <th>123456</th>
      <th>AAA</th>
      <th>管理員</th>
      <th>

          <div class="btn-group">
          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">
          編輯
          </button>
          <div class="dropdown-menu">
          <a class="dropdown-item" href="#!">提升</a>
          <a class="dropdown-item" href="#!">降低</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#!">刪除</a>
          </div>
          </div>

      </th>
    </tr>
  </tbody>
</table>
@endsection

