@extends('frontend.layouts.master')
@section('title', 'newnews')
@section('nav_home', 'active')

@section('content')
<form>
  <div class="form-group">
    <label for="title">標題</label>
    <input class="form-control" type="text" id="title">
    <label for="content">內容</label>
    <textarea  class="form-control" rows="5" id="content"></textarea>
    <label for="date">日期</label>
    <input class="form-control" type="date" id="date">

  </div>
  <button type="submit" class="btn btn-primary">新增</button>
</form>

@endsection
