@extends('frontend.layouts.master')
@section('title', 'newmember')
@section('nav_member', 'active')

@section('content')
<form>
  <div class="form-group">
    <label for="account">學號</label>
    <input class="form-control" type="text" id="account">
    <label for="name">姓名</label>
    <input class="form-control" type="text" id="name">
    <label for="privilege">權限</label>
    <select class="custom-select" id="privilege">
        <option value="1">會員</option>
        <option value="2">管理員</option>
    </select>
    <label for="password">密碼</label>
    <input class="form-control" type="password" id="password">

  </div>
  <button type="submit" class="btn btn-primary">新增</button>
</form>

@endsection
