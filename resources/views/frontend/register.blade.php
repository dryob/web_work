@extends('frontend.layouts.master')
@section('title', 'register')
@section('nav_login', 'active')
@section('content')
<form>
  <div class="form-group">
    <label for="account">學號</label>
    <input class="form-control" type="text" value="" id="account">
    <label for="name">姓名</label>
    <input class="form-control" type="text" value="" id="name">
    <label for="password">密碼</label>
    <input class="form-control" type="password" id="password">

  </div>
  <button type="submit" class="btn btn-primary">註冊</button>
</form>

@endsection
