<!-- https://hackerthemes.com/bootstrap-themes/good-news/#navigation -->
<!-- https://hackerthemes.com/bootstrap-cheatsheet/#container -->
@extends('frontend.layouts.master')
@section('title', 'Home')
@section('nav_home', 'active')
@section('content')
<a href="newnews" class="btn btn-primary">發布消息</a>
<div class="card">
  <div class="card-body">
    <h4 class="card-title">最新消息1</h4>
    <h6 class="card-subtitle mb-2 text-muted">2018/11/05</h6>
    <p class="card-text">
    最新消息1內容    </p>
    <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">編輯</button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#!">編輯</a>
                <a class="dropdown-item" href="#!">刪除</a>
            </div>
    </div>

  </div>
</div>
<div class="card">
  <div class="card-body">
    <h4 class="card-title">最新消息2</h4>
    <h6 class="card-subtitle mb-2 text-muted">2018/11/04</h6>
    <p class="card-text">
    最新消息2內容    </p>
    <div class="btn-group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">編輯</button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#!">編輯</a>
                <a class="dropdown-item" href="#!">刪除</a>
            </div>
    </div>

  </div>
</div>

@endsection
